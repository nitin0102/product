package com.wtc.poc.product.service;

public interface SequenceGeneratorService {
	public Integer getSequence(String sequenceName);
}
