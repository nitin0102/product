package com.wtc.poc.product.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wtc.poc.product.model.Product;
import com.wtc.poc.product.repository.ProductRepositoryMongo;
import com.wtc.poc.product.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

//	@Autowired
//	private ProductRepositoryJPA repository;

	@Autowired
	private ProductRepositoryMongo repository;

	@Override
	public List<Product> getAllProducts() {
		return repository.findAll();
	}

	@Override
	public Product createProduct(Product product) {
		return repository.save(product);
	}

	@Override
	public Product updateProduct(Product product) {
		Optional<Product> optional = repository.findById(product.getId());
		populatePersistenceEntity(product, optional);
		return repository.save(optional.get());
	}

	@Override
	public void deleteProduct(Long id) {
		repository.deleteById(id);
	}

	private void populatePersistenceEntity(Product product, Optional<Product> optional) {
		if (product.getDescription() != null && !product.getDescription().isEmpty()) {
			optional.get().setDescription(product.getDescription());
			optional.get().getAvailability().setQuantity(product.getAvailability().getQuantity());
			;
		}
	}

	@Override
	public Product getProductById(Long id) {
		// TODO Auto-generated method stub
		Optional<Product> optional = repository.findById(id);
		return optional.get();
	}
}
