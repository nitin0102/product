package com.wtc.poc.product.service;

import java.util.List;

import com.wtc.poc.product.model.Product;

public interface ProductService {

	Product createProduct(Product product);

	List<Product> getAllProducts();

	Product updateProduct(Product product);

	void deleteProduct(Long id);

	Product getProductById(Long id);

}
