package com.wtc.poc.product.repository.seq;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class SequenceGenerator {

	@Autowired
	private static MongoOperations mongoOperations;

	public static Integer getSequence(String sequenceName) {

		Query query = new Query(Criteria.where("sequenceName").is(sequenceName));

		Update update = new Update().inc("sequenceValue", 1);

		CollectonSequence collectonSequence = mongoOperations.findAndModify(query, update,
				FindAndModifyOptions.options().returnNew(true).upsert(true), CollectonSequence.class);
		return !Objects.isNull(collectonSequence) ? collectonSequence.getSequenceValue() : 1;
	}

}
