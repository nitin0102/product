package com.wtc.poc.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.wtc.poc.product.model.Product;


@Repository
public interface ProductRepositoryMongo extends MongoRepository<Product, Long> {

}
