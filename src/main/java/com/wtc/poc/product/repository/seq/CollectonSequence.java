package com.wtc.poc.product.repository.seq;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CollectonSequence {
	private String sequenceName;
	private Integer sequenceValue;;

}
