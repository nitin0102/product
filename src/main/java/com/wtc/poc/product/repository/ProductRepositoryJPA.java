package com.wtc.poc.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wtc.poc.product.model.Product;

//@Repository
public interface ProductRepositoryJPA /* extends JpaRepository<Product, Long> */ {

}
