package com.wtc.poc.product.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//@Entity
@Document(collection = "product")
@Setter
@Getter
@ToString
public class Product {
	public static String SEQUENCE_NAME = "order_info_sequence";
	@Id
//	@GeneratedValue
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String description;
	private BigDecimal price;
	@OneToOne(cascade = { CascadeType.ALL })
	private Availability availability;
}
