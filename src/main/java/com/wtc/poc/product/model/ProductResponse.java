package com.wtc.poc.product.model;

import java.util.List;

public class ProductResponse {
	private List<Product> productList;

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public boolean addToList(Product product) {
		return this.productList.add(product);
	}
}
