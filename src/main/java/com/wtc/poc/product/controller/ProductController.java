package com.wtc.poc.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wtc.poc.product.model.Product;
import com.wtc.poc.product.service.ProductService;
import com.wtc.poc.product.service.SequenceGeneratorService;

@CrossOrigin
@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private SequenceGeneratorService seqGenService;

	@PostMapping("/product")
	public ResponseEntity<Product> createProduct(@RequestBody Product product) {
		System.out.println("Creating product:[" + product + "]");
		product.setId(seqGenService.getSequence(Product.SEQUENCE_NAME));
		Product productResponse = productService.createProduct(product);
		return ResponseEntity.ok(productResponse);
	}

	@GetMapping("/products")
	public ResponseEntity<List<Product>> getAllProducts() {
		System.out.println("Get all Products...");
		List<Product> productList = productService.getAllProducts();
		return ResponseEntity.ok(productList);
	}

	@GetMapping("/product/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable Long id) {
		System.out.println("Getting product with id:[" + id + "]");
		Product product = productService.getProductById(id);
		return ResponseEntity.ok(product);
	}

	@PutMapping("/product")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
		System.out.println("Updating product:[" + product + "]");
		Product productResponse = productService.updateProduct(product);
		return ResponseEntity.ok(productResponse);
	}

	@DeleteMapping(("/product/{id}"))
	public ResponseEntity<String> deleteProduct(@PathVariable Long id) {
		System.err.println("Deleting product with id:[" + id + "]");
		productService.deleteProduct(id);
		return ResponseEntity.ok("Product with id:[" + id + "] deleted successfully...");
	}
}