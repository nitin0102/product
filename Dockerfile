FROM openjdk
RUN mkdir -p /home/app
COPY ./target/product-0.0.1-SNAPSHOT.jar /home/app
ENTRYPOINT ["java", "-jar" , "/home/app/product-0.0.1-SNAPSHOT.jar"]